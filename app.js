'use strict'
const path = require('path')
const express = require('express')
const app = express()
app.use(express.static('img'))

app.set('port', (process.env.PORT || 3000));

app.get('/', (req,res)=>{
  const obj = {
    name : 'André Gustavo',
    age : 29,
    email : 'andregb88@gmail.com'
  }

  res.json(obj)
})

app.get("/photo", (req,res)=>{
  res.sendFile(path.join(__dirname + '/photo.html'))
})

app.listen(app.get('port'), ()=>{
  console.log('App Started!')
})
